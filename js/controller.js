var apipath = "https://api-slgarrett9.c9users.io";
var headers = {
    'Content-Type':'application/json'
};

var app = angular.module('bookApp', []);

//FUNCTIONS
function getGoogleBook($http, isbn, callback) {
    if (isbn) {
        $http({
            method: 'GET',
            url: apipath + '/book/search/' + isbn,
            headers: headers
        }).then(function successCallBack(response) {
            callback(response.data);
        },function errorCallBack(response){
            callback([]);
        });
    }   
}

function getBook($http,isbn, callback) {
    $http({
        method: 'GET',
        url: apipath + '/book/' + isbn,
        headers: headers
    }).then(function successCallBack(response) {
        //alert(JSON.stringify(response.data));
        callback(response.data);
    },function errorCallBack(response){
        callback([]);
    });    
}

function getBooks($http,callback) {
    $http({
        method: 'GET',
        url: apipath + '/book/',
        headers: headers
    }).then(function successCallBack(response) {
        //alert(JSON.stringify(response.data));
        callback(response.data);
    },function errorCallBack(response){
        //alert(JSON.stringify(response));
        callback([]);
    });    
}


//BOOKSTORE CONTROLLER
app.controller('bookStore',['$scope', '$http', '$window', function($scope,$http,$window) {
    
    $scope.model = {
        id:0,
        items:[],
        total:0,
        selected:{},
        userid:0,
        username:null,
        password:null
    };
    
    $scope.logout = function() {
        sessionStorage.clear();
        $window.location.href = "login.html";
    };
    
    $scope.login = function() {
        $scope.model.error = "";
        $http({
            method: 'PUT',
            url: apipath + '/account/login',
            headers: headers,
            data:{username:$scope.model.username,password:$scope.model.password}
        }).then(function(response){
            //alert(JSON.stringify(response.data));
            
            if (response.data.userid) {
                sessionStorage.userid = response.data.userid;
                $scope.model.username = null;
                $scope.model.password = null;
                $window.location.href = "store.html"; //SHOULD REDIRECT TO STORE.HTML WHEN LOGGED IN CORRECTLY
                
            } else {
                $scope.model.error = response.data.error;
            }
                //alert(JSON.stringify(response.data));
        });
    };
    
    $scope.signup = function() {
        $scope.model.error = "";
        $http({
            method: 'POST',
            url: apipath + '/account',
            headers: headers,
            data:{username:$scope.model.username,password:$scope.model.password}
        }).then(function(response){
            //alert(JSON.stringify(response));
            if (response.data.id) {
                sessionStorage.userid = response.data.id;
                $scope.model.username = null;
                $scope.model.password = null;
                $window.location.href ="store.html";     
                
            } else {
                alert("signup fail");
                $scope.model.error = response.data.error;
            }
            //alert(JSON.stringify(response.data));
        });
    };
    
    
    
    $scope.getPreviousOrders = function() {
        if (sessionStorage.length == 0) {
            //SEND USER BACK IF NOT LOGGED IN
            $window.location.href ="login.html";
            
        } else {
            //IF LOGGED IT, GET PREVIOUS ORDERS
            $scope.model.username = sessionStorage.username;
            $http({
                method: 'GET',
                url: apipath + '/myorders/' + sessionStorage.userid,
                headers: headers
            }).then(function(response){
                $scope.model.orders = response.data;
            }); 
            
        }
    };
               
    $scope.getTemplate = function (item) {
        if (item.isbn == $scope.model.selected.isbn) return 'edit';
        else return 'display';
    };
    
    $scope.addItem = function() {
        if ($scope.item.isbn && $scope.item.qty) {
            getGoogleBook($http,$scope.item.isbn, function(data) {
                $scope.item.title = data[0].volumeInfo.title;
                
                getBook($http, $scope.item.isbn, function(data) { 
                    $scope.item.unit_price = data[0].price;
                    $scope.item.total_price = $scope.item.unit_price*$scope.item.qty;
                    
                    var item = {
                        isbn:$scope.item.isbn,
                        qty:$scope.item.qty,
                        unit_price:$scope.item.unit_price,
                        total_price:$scope.item.total_price,
                        title:$scope.item.title
                    };
                    $scope.model.items.push(item);
                    $scope.model.total += $scope.item.total_price;
                    $scope.item = ''; 
                    
                });
            });
        }
    };
    
    $scope.reset = function () {
        $scope.model.selected = {};
    };
      
    $scope.edit = function(item) {
        $scope.model.selected = angular.copy(item);    
    };
        //alert($scope.model.username + "\n" + $scope.model.password);
    $scope.delete = function(index) {
        $scope.model.total -= $scope.model.items[index].total_price;
        $scope.model.items.splice(index,1);
    };
    
    $scope.update = function(index) {
        //LATEST PRICE
        getBook($http, $scope.model.selected.isbn, function(data) { 
            var price = data[0].price;
            
            //OLD TOTAL ORDER
            $scope.model.total -= $scope.model.items[index].total_price;
    
            //Updates record
            $scope.model.items[index].unit_price = price;
            $scope.model.items[index].qty = $scope.model.selected.qty;
            $scope.model.items[index].total_price = $scope.model.items[index].unit_price * $scope.model.items[index].qty;
    
            //UPDATES TOTAL ORDER
            $scope.model.total += $scope.model.items[index].total_price;
    
            $scope.reset();
        });
    };
    
    $scope.info = function(item) {
        getGoogleBook($http,item.isbn, function(data) {
            alert(JSON.stringify(data));
        });
    };
    
    $scope.search = function() {
        if ($scope.isbn) {
            getGoogleBook($http,$scope.isbn, function(data) {
               $scope.results = data; 
            });
        }
    };
    
    
    $scope.create = function() {
        if ($scope.model.items.length > 0) {
            var data = {
                userid:sessionStorage.userid,
                items:$scope.model.items
            };
            
            $http({
                method: 'POST',
                url: apipath + '/order',
                headers: headers,
                data:data
            }).then(function successCallBack(response) {
                var orderid = response.data.id;
                $http({
                    method: 'GET',
                    url: apipath + '/myorders/' + sessionStorage.userid,
                    headers: headers
                }).then(function(response){
                    alert("New order placed: " + orderid);
                    $scope.model.items = null;
                    $scope.model.total = 0;
                    $scope.model.orders = response.data;
                });
                
            });
            
        } else {
            alert("Basket empty!");
            
        }
    };

}]);

//BOOKSTORE ADMIN CONTROLLER
app.controller('bookAdmin', ['$scope','$http', function($scope, $http) {
    $scope.model = {
        books:[],
        selected:{}
    };
    
    getBooks($http, function(data) {
        $scope.model.books = data;
        //alert(JSON.stringify($scope.model.books));
    });
    
    $scope.getTemplate = function (book) {
        if (book.isbn == $scope.model.selected.isb) return 'edit';
        else return 'display';
    };
    
    $scope.reset = function () {
        $scope.model.selected = {};
    };
      
    $scope.edit = function(book) {
        $scope.model.selected = angular.copy(book);    
    };
    
    $scope.update = function() {
        var data = {
            quantity:$scope.model.selected.quantity,
            price:$scope.model.selected.price,
            title:$scope.model.selected.title,
            isbn:$scope.model.selected.isbn
        };
        
        $http({
            method: 'PUT',
            url: apipath + '/book/' + $scope.model.selected.id,
            headers: headers,
            data:data
        }).then(function (response) {
            $scope.reset();
            getBooks($http, function(data) {
                $scope.model.books = data;
            }); 
        });
    };
    
    $scope.delete = function(book) {
        $http({
            method: 'DELETE',
            url: apipath + '/book/' + book.id,
            headers: headers
        }).then(function (response) {
            getBooks($http, function(data) {
                $scope.model.books = data;
            }); 
        });
    };
    
    $scope.info = function(book) {
        getGoogleBook($http,book.isbn, function(data) {
            alert(JSON.stringify(data));
        });
    };
    
    $scope.create = function() {
        if ($scope.book.isbn && $scope.book.quantity) {
            
            getGoogleBook($http,$scope.book.isbn, function(data) {
                $scope.book.title = data[0].volumeInfo.title;
            
                var data = {
                    title:$scope.book.title,
                    isbn:$scope.book.isbn,
                    quantity:$scope.book.quantity,
                    price:$scope.book.price
                };
                //alert(JSON.stringify(data));
                
                $http({
                    method: 'POST',
                    url: apipath + '/book',
                    headers: headers,
                    data:data
                }).then(function successCallBack(response) {
                    $scope.book = '';
                    //alert(JSON.stringify(response.data));
                    getBooks($http, function(data) {
                        $scope.model.books = data;
                    });
                },function errorCallBack(response){
                    alert(JSON.stringify(response.data));
                });
            });
        }
    };
    
}]);